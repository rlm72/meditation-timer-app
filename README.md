A minimalist meditation timer app for Android.

![Screenshot](/meditation_app_screenshot.png?raw=true)

Timer can be set in 5 minute increments. A woodblock sound plays at 5 minute intervals, once for each interval that has passed. A triangle plays with increasing volume when the timer finishes.

Written in Kotlin.