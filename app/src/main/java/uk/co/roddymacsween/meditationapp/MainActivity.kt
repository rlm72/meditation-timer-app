package uk.co.roddymacsween.meditationapp

import android.content.Intent
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.media.SoundPool
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.support.annotation.RequiresApi
import android.text.format.DateUtils.formatElapsedTime
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import java.lang.IllegalStateException

const val TIME_REMAINING_DELTA: Long = 5 * 60 * 1000    // change in timer value in milliseconds when up/down buttons pressed
const val INITIAL_TIMER_VALUE: Long = 15 * 60 * 1000    // in milliseconds

enum class TimerState {
    GOING, PAUSED, STOPPED
}

abstract class PausableCountDownTimer(var millisInFuture : Long, val countDownInterval : Long) {
    var millisRemaining : Long = millisInFuture
    var state = TimerState.STOPPED

    var countDownTimer : CountDownTimer = createCountdownTimer()

    private fun createCountdownTimer() : CountDownTimer {
        return object: CountDownTimer(millisRemaining, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                millisRemaining = millisUntilFinished
                this@PausableCountDownTimer.onTick(millisUntilFinished)
            }

            override fun onFinish() {
                this@PausableCountDownTimer.onFinish()
                state = TimerState.STOPPED
            }
        }
    }

    abstract fun onTick(millisUntilFinished : Long)
    abstract fun onFinish()
    open fun onStart() {}
    open fun onResume() {}
    open fun onPause() {}
    open fun onCancel() {}
    open fun onReset() {}
    open fun onTimeChange() {}

    fun cancel() {
        countDownTimer.cancel()
        millisRemaining = 0
        state = TimerState.STOPPED
        onCancel()
    }

    fun reset() {
        if (state == TimerState.STOPPED) {
            millisRemaining = millisInFuture
            onReset()
        } else {
            throw IllegalStateException("Cannot only reset timer in stopped state")
        }
    }

    @Synchronized fun go() {
        countDownTimer = createCountdownTimer()
        countDownTimer.start()
        state = TimerState.GOING
    }

    @Synchronized fun start() : PausableCountDownTimer {
        if (state == TimerState.STOPPED) {
            millisRemaining = millisInFuture
            go()
            onStart()
        } else {
            throw IllegalStateException("Can only start timer in stopped state (use `resume` to restart a paused timer)")
        }
        return this
    }

    fun pause() {
        if (state == TimerState.GOING) {
            countDownTimer.cancel()
            state = TimerState.PAUSED
            onPause()
        } else {
            throw IllegalStateException("Can only pause timer in going state")
        }
    }

    @Synchronized fun resume() : PausableCountDownTimer {
        if (state == TimerState.PAUSED) {
            go()
            onResume()
        } else {
            throw IllegalStateException("Can only resume timer in paused state")
        }
        return this
    }

    @Synchronized fun increaseTime(millisToIncrease : Long) {
        // increment both the current time remaining and time remaining after reset
        millisInFuture += millisToIncrease
        millisRemaining += millisToIncrease
        if (state == TimerState.GOING) {
            countDownTimer.cancel()
            go()
        }
        onTimeChange()
    }
}

class MainActivity : AppCompatActivity() {
    lateinit var timerText: TextView
    lateinit var timer: PausableCountDownTimer
    lateinit var playButton: ImageButton

    var playIntervalSounds = true
    var soundInterval = 60 * 5        // in number of ticks

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        timerText = findViewById(R.id.timer_text)
        timerText.text = formatElapsedTime(INITIAL_TIMER_VALUE / 1000)
        
        playButton = findViewById(R.id.play_button)

        val woodBlockPlayer = SoundPool.Builder().build()
        val woodBlockID = woodBlockPlayer.load(applicationContext, R.raw.wood_block, 1)
        val trianglePlayer = MediaPlayer.create(applicationContext, R.raw.end_triangle)

        timer = object: PausableCountDownTimer(INITIAL_TIMER_VALUE, 1000) {
            var tickNumber = 0
            var intervalCounts = 0
            override fun onTick(millisUntilFinished: Long) {
                tickNumber++
                timerText.text = formatElapsedTime(millisUntilFinished / 1000)
                if (playIntervalSounds && tickNumber % soundInterval == 0 && millisUntilFinished > 10000) {
                    woodBlockPlayer.play(woodBlockID, 0.1f, 0.1f, 0, intervalCounts, 1f)
                    intervalCounts++
                }
            }

            override fun onFinish() {
                trianglePlayer.start()
                playButton.setImageResource(R.drawable.ic_play_arrow_black_96dp)
            }

            override fun onStart() { playButton.setImageResource(R.drawable.ic_pause_black_96dp) }
            override fun onResume() { playButton.setImageResource(R.drawable.ic_pause_black_96dp) }
            override fun onPause() { playButton.setImageResource(R.drawable.ic_play_arrow_black_96dp) }
            override fun onReset() {
                playButton.setImageResource(R.drawable.ic_play_arrow_black_96dp)
                tickNumber = 0
                intervalCounts = 0
                timerText.text = formatElapsedTime(this.millisInFuture / 1000)
            }

            override fun onTimeChange() {
                timerText.text = formatElapsedTime(this.millisRemaining / 1000)
            }
        }
    }

    fun playButtonPress(view: View) {
        when (timer.state) {
            TimerState.STOPPED -> {
                timer.start()
            }
            TimerState.PAUSED -> {
                timer.resume()
            }
            TimerState.GOING -> {
                timer.pause()
            }
        }
    }

    fun stopButtonPress(view: View) {
        timer.cancel()
        timer.reset()
    }

    fun timeIncreaseButtonPress(view: View) {
        timer.increaseTime(TIME_REMAINING_DELTA)
    }

    fun timeDecreaseButtonPress(view: View) {
        if (timer.millisInFuture > TIME_REMAINING_DELTA && timer.millisRemaining > TIME_REMAINING_DELTA)
        timer.increaseTime(-TIME_REMAINING_DELTA)
    }
}
